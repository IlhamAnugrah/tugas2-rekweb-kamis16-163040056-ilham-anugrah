<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_orders extends CI_Model {

	public function process() {
		// Membuat invoice
		$invoice = array(
			'date'     => date('Y-m-d H:i:s'),
			'due_date' => date('Y-m-d H:i:s', mktime( date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
			'user_id'  => $this->get_logged_user_id(),
			'status'   => 'unpaid'
		);
		$this->db->insert('invoices', $invoice);
		$invoice_id = $this->db->insert_id();

		$this->load->Model('Model_barang');
		$stok = 0;
		// Mengambil satu per satu item 
		foreach ( $this->cart->contents() as $item ) {
			$getData = $this->Model_barang->get_barang_by_id($item['id']);
			$stok = $getData['jumlah']-$item['qty'];
			$this->Model_barang->update_stock($stok, $item['id']);
			$data = array(
				'invoice_id'	=> $invoice_id,
				'product_id'	=> $item['id'],
				'product_name'	=> $item['name'],
				'qty'			=> $item['qty'],
				'price'			=> $item['price']
			);
			$this->db->insert('orders', $data);
		}
		return true;
	}


	public function get_invoice_by_id($invoice_id) {
		$hasil = $this->db->where('id', $invoice_id)->limit(1)->get('invoices');

		if ( $hasil->num_rows() > 0 ) {
			return $hasil->row();
		} else {
			return false;
		}
	}

	public function get_orders_by_invoice($invoice_id) {
		$hasil = $this->db->where('invoice_id', $invoice_id)->get('orders');
		
		if ( $hasil->num_rows() > 0 ) {
			return $hasil->result();
		} else {
			return false;
		}
	}

	public function get_logged_user_id() {
		$hasil = $this->db
					  ->select('id_login')
					  ->where('username', $this->session->userdata('username'))
					  ->limit(1)
					  ->get('login_session');

		if ( $hasil->num_rows() > 0 ) {
			return $hasil->row()->id_login;
		} else {
			return 0;
		}
	}

	public function get_produk_all()
	{
		$query = $this->db->get('tabel_produk');
		return $query->result_array();
	}
	
}