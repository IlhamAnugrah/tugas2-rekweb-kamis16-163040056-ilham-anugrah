<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_barang extends CI_Model {

    public function tambah($barang) 
    {
        return $this->db->insert('tabel_produk', $barang);
    }

    public function hapus($id)
    {
        return $this->db->where('id_barang', $id)
                        ->delete('tabel_produk');
    }

    public function get_barang_by_id($id)
    {
        $query = $this->db->get_where('tabel_produk', ['id_barang' => $id]);
        return $query->row_array();
    }

    public function ubah($barang)
    {
        $this->db->where('id_barang', $barang['id_barang']);
        return $this->db->update('tabel_produk', $barang);
    }	

    public function update_stock($stok, $id) 
    {
        $this->db->where('id_barang', $id);
        return $this->db->update('tabel_produk', array( 'jumlah' => $stok ));
    }
	
}
