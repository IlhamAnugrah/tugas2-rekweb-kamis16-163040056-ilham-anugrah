<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ( !$this->session->userdata('username') ) {
			redirect('login');
		}
		$this->load->model('Model_orders');
	}

	public function index() {
		$is_processed = $this->Model_orders->process();
		if ( $is_processed ) {
			$this->cart->destroy();
			redirect('order/success');
		} else {
			$this->session->set_flashdata('error', 'Failed to processed your order, please try again!');
			redirect('shopping/cart');
		}
	}

	public function success() {
        $data['judul'] = 'Shopping';
        
		$data['content'] = 'shopping/order_success';
		$this->load->view('templates/template', $data);
	}

}