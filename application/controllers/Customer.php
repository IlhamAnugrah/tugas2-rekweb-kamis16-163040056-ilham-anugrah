<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Model_customer');
	}

	public function index() {
		
	}

	public function payment_confirmation($invoice_id = 0) {
		$data['judul'] = 'Shopping';

		$this->form_validation->set_rules('invoice_id', 'Invoice ID', 'required|integer');
		$this->form_validation->set_rules('amount', 'Amount Transfered', 'required|integer');

		if( $this->form_validation->run() == FALSE ) {
			if ( $this->input->post('invoice_id') ) {
				$data['invoice_id'] = set_value('invoice_id');
			} else {
				$data['invoice_id'] = $invoice_id;
            }
            $data['content'] = 'customer/payment_confirmation';
            $this->load->view('templates/template', $data);
		} else {
			$isValid = $this->Model_customer->mark_invoice_confirmed(set_value('invoice_id'), set_value('amount'));

			if( $isValid ) {
				$this->session->set_flashdata('message', 'Thank you.. for making payment');
				redirect('customer/shopping_history');
			} else {
				$this->session->set_flashdata('error', 'Wrong amount or Invoice ID, try again!');
				redirect('customer/payment_confirmation/' . set_value('invoice_id'));
			}
		}
	}

	public function shopping_history() {
		$data['judul'] = 'Shopping';

		$user = $this->session->userdata('username');
        $data['history'] = $this->Model_customer->get_shopping_history($user);
        
        $data['content'] = 'customer/shopping_history';
        $this->load->view('templates/template', $data);
	}

}