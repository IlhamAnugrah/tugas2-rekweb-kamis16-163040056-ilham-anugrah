<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if($this->session->userdata('level') != '1') {
            $this->session->set_flashdata('error', 'Sorry you are not logged in!');
            redirect('login');
        }
        $this->load->model('Model_barang');
        
    }
    
    public function index() {
        $data['judul'] = 'Halaman Administrator';

        $data['content'] = 'admin/index';
		$this->load->view('templates/template', $data);
    }

}