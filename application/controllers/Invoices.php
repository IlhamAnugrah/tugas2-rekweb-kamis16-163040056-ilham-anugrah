<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if($this->session->userdata('level') != '1') {
            $this->session->set_flashdata('error', 'Sorry you are not logged in!');
            redirect('login');
        }
        $this->load->model('Model_orders');
    }
    
    public function index() {
        $data['judul'] = 'Administrator Page';

        if( $this->input->post() ) {
            $keyword = $this->input->post('search');
            $array = [
                'status' => $keyword,
                'id'     => $keyword
            ];
            $this->db->or_like($array);
        } 
        $query = $this->db->get('invoices');
        $data['invoice'] = $query->result_array();
        
        $data['content'] = 'admin/invoices';
		$this->load->view('templates/template', $data);
    }

    public function detail($invoice_id) {
        $data['judul'] = 'Halaman Invoice';
        $data['invoice'] = $this->Model_orders->get_invoice_by_id($invoice_id);
        $data['orders'] = $this->Model_orders->get_orders_by_invoice($invoice_id);

        $data['content'] = 'admin/invoices_detail';
		$this->load->view('templates/template', $data);
    }

}