<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
    
    public function index() {
        $data['judul'] = 'Administrator Page';

        if( $this->input->post() ) {
            $keyword = $this->input->post('search');
            $array = [
                'nama_barang' => $keyword,
                'merk' => $keyword,
                'kategori' => $keyword,
                'jumlah' => $keyword,
                'garansi' => $keyword
            ];
            $this->db->or_like($array);
        } 
        $query = $this->db->get('tabel_produk');
        $data['barang'] = $query->result_array();
        
        $data['content'] = 'admin/barang';
		$this->load->view('templates/template', $data);
    }

    public function tambah() {
        $data['judul'] = 'Add Item Page';

        $this->form_validation->set_rules('nama_barang', 'Nama of Item', 'required|alpha_numeric_spaces');
		$this->form_validation->set_rules('merk', 'Brand', 'required');
		$this->form_validation->set_rules('kategori', 'Category', 'required');
		$this->form_validation->set_rules('jumlah', 'Quantity', 'required|integer');
		$this->form_validation->set_rules('harga', 'Price', 'required|integer');
		$this->form_validation->set_rules('garansi', 'Warranty', 'required|alpha_numeric_spaces');
		$this->form_validation->set_rules('gambar', 'Picture', 'required');
        
        if ( $this->form_validation->run() == FALSE ) {
            $data['content'] = 'admin/tambah';
            $this->load->view('templates/template', $data);
        } else {

        if( $this->input->post() ) {
            
            $barang['nama_barang'] = $this->input->post('nama_barang');
            $barang['merk'] = $this->input->post('merk');
            $barang['kategori'] = $this->input->post('kategori');
            $barang['jumlah'] = $this->input->post('jumlah');
            $barang['harga'] = $this->input->post('harga');
            $barang['garansi'] = $this->input->post('garansi');
            $barang['gambar'] = $this->input->post('gambar');
            $this->load->model('Model_barang');

            if( $this->Model_barang->tambah($barang) > 0 ) {
                $this->session->set_flashdata('info_tambah', 'Data berhasil ditambahkan!');
            } else {
                $this->session->set_flashdata('info_tambah', 'Data gagal ditambahkan!');
            }
        }
            redirect('barang');
            
        }
    }

    public function hapus($id)
    {
        $this->load->model('Model_barang');

        if( $this->Model_barang->hapus($id) > 0 ) {
            $this->session->set_flashdata('info_hapus', 'Data berhasil dihapus!');
        } else {
            $this->session->set_flashdata('info_hapus', 'Data gagal dihapus!');
        }
        redirect('barang');
    }


    public function ubah($id)
    {
        $data['judul'] = 'Change Item Page';

        $this->load->model('Model_barang');
        $data['barang'] = $this->Model_barang->get_barang_by_id($id);
        
        if( $this->input->post() ) {
            $barang['nama_barang'] = $this->input->post('nama_barang');
            $barang['merk'] = $this->input->post('merk');
            $barang['kategori'] = $this->input->post('kategori');
            $barang['jumlah'] = $this->input->post('jumlah');
            $barang['harga'] = $this->input->post('harga');
            $barang['garansi'] = $this->input->post('garansi');
            $barang['gambar'] = $this->input->post('gambar');
            $barang['id_barang'] = $id;

            if( $this->Model_barang->ubah($barang) > 0 ) {
                $this->session->set_flashdata('info_ubah', 'Data berhasil diubah!');
            } else{
                $this->session->set_flashdata('info_ubah', 'Data gagal diubah!');
            }
            redirect('barang');
        }

        $data['content'] = 'admin/ubah';
		$this->load->view('templates/template', $data);
    }

}
