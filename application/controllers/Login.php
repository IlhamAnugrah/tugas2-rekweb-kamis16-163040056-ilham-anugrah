<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Login extends CI_Controller {

	public function index() {
		$data['judul'] = 'Halaman Login';

		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');

		if( $this->form_validation->run() == FALSE ) {
            $data['content'] = 'form_login';
		    $this->load->view('templates/template', $data);
		} else {
			$this->load->model('model_users');
			$valid_user = $this->model_users->check_credential();

			if ($valid_user == FALSE) {
				$this->session->set_flashdata('error', 'Wrong username / password');
				redirect('login');
			} else {
				$this->session->set_userdata('username', $valid_user->username);
				$this->session->set_userdata('level', $valid_user->level);

				switch ($valid_user->level) {
					case 1: redirect('admin'); break;
					case 2: redirect(base_url()); break;
					default: break;
				}
			}
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('login');
	}
	
}	