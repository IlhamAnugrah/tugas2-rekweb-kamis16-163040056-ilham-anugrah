<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Model_shopping');
    }

    public function index() {
        $data['judul'] = 'Shopping';

        $data['barang'] = $this->Model_shopping->get_produk_all();
        
        $data['content'] = 'shopping/index';
		$this->load->view('templates/template', $data);
    }

    public function add_to_cart($id) {
		$this->load->model('Model_barang');

		$barang = $this->Model_barang->get_barang_by_id($id);

		$data = array(
	        'id'      => $barang['id_barang'],
	        'qty'     => 1,
	        'price'   => $barang['harga'],
	        'name'    => $barang['nama_barang']
	);

	$this->cart->insert($data);
		redirect('shopping');
	}

	public function cart() {
		$data['judul'] = 'Shopping';
		
		$data['content'] = 'shopping/show_cart';
		
		$this->load->view('templates/template', $data);
	}

	public function clear_cart() {
		$this->cart->destroy();
		redirect('shopping');
	}

	public function update_cart() {
		$data['judul'] = 'Shopping';

		$this->load->model('Model_barang');
		$i = 1;
		
		foreach ( $this->cart->contents() as $barang ) {
			$getData = $this->Model_barang->get_barang_by_id($barang["id"]);
			if ( $getData['jumlah'] < $_POST[$i . 'qty'] ) {
				$this->session->set_flashdata('error', 'Stock is not enough');
				redirect('shopping/cart');
			} else {
				$this->cart->update(array('rowid' => $barang['rowid'], 'qty' => $_POST[$i . 'qty']));
				$i++;
			}
		}
		$data['content'] = 'shopping/show_cart';
		$this->load->view('templates/template', $data);
	}

	function delete_product_cart($rowid) 
	{
		if ($rowid=="all") {
			$this->cart->destroy();
		} else {
			$data = array('rowid' => $rowid,
							'qty' =>0);
			$this->cart->update($data);
		}
		redirect('shopping/cart');
	}
    
}