<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <div class="logo">
        <a href="" class="simple-text logo-normal">
          Administrator
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item   ">
            <a class="nav-link" href="<?= site_url('admin') ?>">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <!-- your sidebar here -->
          <li class="nav-item  ">
            <a class="nav-link" href="<?= site_url('shopping') ?>">
              <i class="material-icons">shopping_cart</i>
              <p>Shopping</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= site_url('barang/tambah') ?>" target="_blank">
              <i class="material-icons">note_add</i>
              <p>Add Data</p>
            </a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="<?= site_url('barang') ?>">
              <i class="material-icons">content_paste</i>
              <p>Product Table</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?= site_url('invoices') ?>">
              <i class="material-icons">assignment</i>
              <p>Invoices Table</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Invoices Table</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">notifications</i> Notifications
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
                </div>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          <div class="row">
            <div class="col-md-12">

            <!-- Breadcumb -->
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item "><a href="<?= site_url('admin') ?>">Home</a></li>
                <li class="breadcrumb-item "><a href="<?= site_url('invoices') ?>">Invoice List</a></li>
                <li class="breadcrumb-item active" aria-current="page">Invoices Detail</li>
              </ol>
            </nav>
            
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Invoices Detail</h4>
                  <p class="card-category"> This is invoice detail</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">

                  <h3>Items ordered in Invoice <b>#<?= $invoice->id ?> </b></h3>
                  <table class="table table-hover">
                    <thead class=" text-center text-primary">
                    <tr>
                        <th style="font-weight: bold;">Product ID</th>
                        <th style="font-weight: bold;">Product Name</th>
                        <th style="font-weight: bold;">Quantity</th>
                        <th style="font-weight: bold;">Price</th>
                        <th style="font-weight: bold;">Subtotal</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">

                    <?php 
                        $total = 0;
                        foreach ($orders as $order) : 
                        $subtotal = $order->qty * $order->price;
                        $total += $subtotal;
				             ?>
                        <tr>
                            <td><?= $order->product_id ?></td>
                            <td><?= $order->product_name ?></td>
                            <td><?= $order->qty ?></td>
                            <td>Rp. <?= number_format( $order->price,0,",","." ) ?></td>
                            <td>Rp. <?= number_format( $subtotal,0,",","." ) ?></td>
                        </tr>

                        <?php endforeach; ?>

                    </tbody>
                    <tfoot>
                        <tr class="text-center">
                            <td style="font-weight: bold;" colspan="4" align="center">Total</td>
                            <td style="font-weight: bold;">Rp. <?= number_format( $total,0,",","." ) ?></td>
                        </tr>
                    </tfoot>
                  </table>

                  </div>
                </div>
              </div>  
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="">
                  REKWEB
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script> built by
            <a href="" target="_blank">Selembung</a>
          </div>
          <!-- your footer here -->
        </div>
      </footer>
    </div>
  </div>