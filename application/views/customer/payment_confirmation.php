<nav class="navbar navbar-expand-lg bg-primary">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url() ?>"><b>Electronical Shop</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">

    <?php if ( $this->session->userdata('username') ) { ?>
      
        <li class="nav-item active">
          <?= anchor('customer/payment_confirmation/', 'Payment Confirmation', [
                    'class' => 'nav-link',
                    'role'  => 'a'
          ]) ?>
        </li>
        <li class="nav-item active">
          <?= anchor('customer/shopping_history/', 'History', [
                     'class' => 'nav-link',
                     'role'  => 'link'
          ]) ?>
        </li>
        <?php } ?>
        <li class="nav-item active">
          <a class="nav-link" href="<?= site_url('shopping/cart') ?>">Shopping Cart
            <i class="material-icons">shopping_cart</i>
            <span class="notification"><?= $this->cart->total_items() ?></span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <a class="dropdown-item" href="#">Profile</a>
            <a class="dropdown-item" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container mt-5"  style="height: 100%; padding-top: 100px;">
	<div class="row">
		<div class="col-md-4 mx-auto">
		<div class="card card-nav-tabs">
			<h4 class="card-header card-header-info">Payment Confirmation</h4>
		<div class="card-body">

		  <?php if( validation_errors() ) : ?>
        <div class="alert alert-danger alert-dismissible fade show mb-5" role="alert">
          <?= validation_errors(); ?>
          <button type="button" class="close mt-2" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php endif ?>

      <?php if( $this->session->flashdata('error') ) : ?>
        <div class="alert alert-danger alert-dismissible fade show mb-5" role="alert">
          <?= $this->session->flashdata('error') ?>
          <button type="button" class="close mt-2" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php endif ?>

			<?= form_open("customer/payment_confirmation/"); ?>
			<div class="form-group">
				<label for="username" class="bmd-label-floating">Invoice ID</label>
				<input type="text" class="form-control" name="invoice_id" value="<?= ($invoice_id != 0 ? $invoice_id:'') ?>">
			</div>
			<div class="form-group">
				<label for="username" class="bmd-label-floating">Amount Transfered</label>
				<input type="number" class="form-control" name="amount">
			</div>
			<button type="submit" class="btn btn-primary pull-right">Confirm My Payment</button>

			<?php echo form_close(); ?>
		</div>
		</div>

		</div>
	</div>
</div>