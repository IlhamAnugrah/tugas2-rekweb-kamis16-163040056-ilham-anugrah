<nav class="navbar navbar-expand-lg bg-primary">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url() ?>"><b>Electronical Shop</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">

    <?php if ( $this->session->userdata('username') ) { ?>
      
        <li class="nav-item active">
          <?= anchor('customer/payment_confirmation/', 'Payment Confirmation', [
                    'class' => 'nav-link',
                    'role'  => 'a'
          ]) ?>
        </li>
        <li class="nav-item active">
          <?= anchor('customer/shopping_history/', 'History', [
                     'class' => 'nav-link',
                     'role'  => 'link'
          ]) ?>
        </li>
        <?php } ?>
        <li class="nav-item active">
          <a class="nav-link" href="<?= site_url('shopping/cart') ?>">Shopping Cart
            <i class="material-icons">shopping_cart</i>
            <span class="notification"><?= $this->cart->total_items() ?></span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <a class="dropdown-item" href="#">Profile</a>
            <a class="dropdown-item" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container mt-5">
	<div class="row">

		<div class="card">
			<div class="card-header card-header-info">
				<h4 class="card-title ">Shopping History</h4>
				<p class="card-category"> This is shopping history</p>
			</div>
			<div class="card-body">
				<div class="table-responsive">

		<?php if( $history != false ) : ?>
    <?php if( $this->session->flashdata('message') ) : ?>
      <div class="alert alert-success alert-dismissible fade show mb-5" role="alert">
        <?= $this->session->flashdata('message') ?>
        <button type="button" class="close mt-2" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif ?>

		<table class="table table-bordered table-hover">
			<thead class="text-primary text-center">
				<tr>
					<th class="text-center" style="font-weight: bold;">Invoice ID</th>
					<th class="text-center" style="font-weight: bold;">Invoice Date</th>
					<th class="text-center" style="font-weight: bold;">Due Date</th>
					<th class="text-center" style="font-weight: bold;">Total Amount</th>
					<th class="text-center" style="font-weight: bold;">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach ($history as $row):
				?>
				<tr>
					<td align="center"><?= $row->id ?></td>
					<td align="center"><?= $row->date ?></td>
					<td align="center"><?= $row->due_date ?></td>
					<td align="center" style="font-weight: bold;">Rp. <?= number_format( $row->total,0,",","." ) ?></td>
					<td align="center">
						<button class="btn btn-danger btn-sm"><?= $row->status ?></button>
						<?php if ( $row->status == 'unpaid' ) { ?>
							<?= anchor('customer/payment_confirmation/' . $row->id, 'Confirm Payment', 
							array('class' => 'btn btn-primary btn-sm')
								) ?>
						<?php } ?> 
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		</div>
    </div>
</div>

		<?php else : ?>
			<p>There are no shopping history for you! <?= anchor('/', 'Shop Now') ?></p>
		<?php endif ?>

	</div>
</div>