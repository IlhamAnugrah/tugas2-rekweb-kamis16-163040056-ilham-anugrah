<nav class="navbar navbar-expand-lg bg-primary">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url() ?>"><b>Electronical Shop</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">

    <?php if ( $this->session->userdata('username') ) { ?>
      
        <li class="nav-item active">
          <?= anchor('customer/payment_confirmation/', 'Payment Confirmation', [
                    'class' => 'nav-link',
                    'role'  => 'a'
          ]) ?>
        </li>
        <li class="nav-item active">
          <?= anchor('customer/shopping_history/', 'History', [
                     'class' => 'nav-link',
                     'role'  => 'link'
          ]) ?>
        </li>

        <?php } ?>

        <li class="nav-item active">
          <a class="nav-link" href="<?= site_url('shopping/cart') ?>">Shopping Cart
            <i class="material-icons">shopping_cart</i>
            <span class="notification"><?= $this->cart->total_items() ?></span>
          </a>
        </li>

        <?php if ( $this->session->userdata('username') ) { ?>

        <li class="nav-item dropdown">
          <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <a class="dropdown-item" href=""><?= $this->session->userdata('username') ?></a>
            <a class="dropdown-item" href="">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
          </div>
        </li>
        
        <?php } else { ?>

        <li class="nav-item dropdown">
          <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <a class="dropdown-item" href="#">Profile</a>
            <a class="dropdown-item" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log in</a>
          </div>
        </li>

        <?php } ?>
        
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  <h2 class="text-center mt-5">List of Product</h2>
    <div class="row mt-5 mb-5">
      
      <?php foreach ($barang as $key) : ?>

        <div class="col-md-4">
          <div class="card img-thumbnail" style="width: ;">
            <img class="card-img-top" src="<?= base_url('assets/img/') . $key['gambar']; ?>" alt="Card image cap">
            <div class="card-header card-header-info mt-3">
                <h4 class="card-title text-center"><?= $key['nama_barang'] ?></h4>
            </div>
            <div class="card-body">
              <p class="card-text">Brand: <?= $key['merk'] ?></p>
              <p class="card-text">Warranty: <?= $key['garansi'] ?></p>
              <p class="card-text">Harga: <?= number_format( $key['harga'],0,",","." ) ?></p>
              <p class="card-text">Stock: <?= $key['jumlah'] ?></p>

              <?php if( $key['jumlah'] > 0 ) { ?>
              
              <p class="card-text text-center">
              <?= anchor('shopping/add_to_cart/' . $key['id_barang'], 'Buy' , [
                         'class' => 'btn btn-primary btn-round',
                         'role'  => 'button',
                         'rel'   => 'tooltip', 
                         'title' => 'Add to cart'
              ]) ?>
              </p>
              
              <?php } else { ?>

              <p class="card-text text-center">
              <?= anchor('', 'Buy' , [
                         'class' => 'btn btn-default btn-round disabled',
                         'role'  => 'button'
              ]) ?>
              </p>

              <?php } ?>
            </div>
          </div>
        </div>

        <?php endforeach; ?>

    </div>
  </div>
</div>

