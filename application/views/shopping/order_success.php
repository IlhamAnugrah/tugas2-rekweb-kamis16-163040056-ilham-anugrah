<nav class="navbar navbar-expand-lg bg-primary">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url() ?>"><b>Elecronical Shop</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">

    <?php if ( $this->session->userdata('username') ) { ?>
      
        <li class="nav-item active">
          <?= anchor('customer/payment_confirmation/', 'Payment Confirmation', [
                    'class' => 'nav-link',
                    'role'  => 'a'
          ]) ?>
        </li>
        <li class="nav-item active">
          <?= anchor('customer/shopping_history/', 'History', [
                     'class' => 'nav-link',
                     'role'  => 'link'
          ]) ?>
        </li>
        <?php } ?>
        <li class="nav-item active">
          <a class="nav-link" href="<?= site_url('shopping/cart') ?>">Shopping Cart
            <i class="material-icons">shopping_cart</i>
            <span class="notification"><?= $this->cart->total_items() ?></span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <a class="dropdown-item" href="#">Profile</a>
            <a class="dropdown-item" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container mt-5">
	<div class="row" style="padding-top: 100px;">
		
		<div class="alert alert-success  mx-auto" role="alert">
		  <h4 class="alert-heading" style="font-weight: bold;">Thank you!</h4>
		  <p>Your order will be processed soon.</p>
		  <hr>
		  <p class="mb-0">We will contact you as soon as possible for the information.</p>
		</div>

	</div>
</div>